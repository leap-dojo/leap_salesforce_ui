# LeapSalesforce Ui

UI capability built on top of `leap_salesforce` gem to aid getting started
with UI testing in Salesforce fast and easy.

> This only works in Lightning view, not classic. If you need UI testing of classic, raise an issue

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'leap_salesforce_ui'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install leap_salesforce_ui

## Usage

This gem is an extension to [leap salesforce](https://gitlab.com/leap-dojo/leap_salesforce)
Setup `leap_salesforce` with `leap_salesforce init` and use the retrospective rake tasks to create soql objects. 

Add the line `require 'leap_salesforce_ui/rake'` to your `Rakefile`. Then run the Rake task `rake leaps:create_poms`
which will generate page objects based upon the `soql_objects`

Set the current UI user to be used with the `LeapSalesforce.ui_user` attribute.

E.g

```ruby
LeapSalesforce.ui_user = 'test.user@salesforce.com'
```

See [leap salesforce test users section](https://gitlab.com/leap-dojo/leap_salesforce#test-users)
for details on ways to set test users.

### Example spec_helper

Following is example code that can be used to setup and teardown each test.

```ruby
require 'leap_salesforce_ui'
RSpec.configure do |config|
  LeapSalesforce.ui_user = LeapSalesforce::Users.list.first
  config.before(:each) do |example|
    # Start browser with test name as metadata
    LeapSalesforce.browser example.full_description
  end

  config.after(:each) do |example|
    # Take screenshot on failure
    screenshot = "logs/#{tidy_description(example.full_description.to_s)}_failure.png"
    LeapSalesforce.browser.screenshot.save screenshot if example.exception
    # Use a fresh browser for each test
    LeapSalesforce.close_browser 
  end

  # Tidy RSpec example description so it's suitable for a screenshot name
  # @return [String] Description of test suitable for file name
  def tidy_description(rspec_description)
    rspec_description.delete("#<RSpec::Core::Example").delete('>".:{}').delete("=").delete(" ")
  end
end
```

### Populate form using label in metadata

Following is a walk through of one of the tests. If you've copied the above code into `spec/spec_helper.rb`
you should be able to copy this code into a new test file and execute it.

> It may or may not work depending on how the fields Contact has in your org

```ruby
RSpec.describe "Creating record" do
  let(:first_name) { Faker::Name.first_name }
  let(:last_name) { Faker::Name.last_name }
  let(:salutation) { Contact::Salutation.sample }
  let(:other_street) { Faker::Address.full_address }
  it "populate form and save" do
    account = FactoryBot.create(:account)
    CreateContactPage.visit.submit_with({ "First Name" => first_name,
                                          last_name: last_name,
                                          salutation: salutation,
                                          other_street: other_street,
                                          account_id: account.account_name })
    contact_id = ViewContactPage.id
    @contact = Contact.find(id: contact_id)
    expect(@contact.first_name).to eq first_name
    expect(@contact.last_name).to eq last_name
    expect(@contact.salutation).to eq salutation
    expect(@contact.other_street).to eq other_street
    expect(@contact.account_id).to eq account.id
  end
end
```

One does not have to identify how to recognise first_name, last_name, etc. 
From the metadata it works out what kind of field (textfield, textarea, reference) and then 
finds the corresponding label and uses that to fill in the field.

This test then uses the API to setup a dependent account (through FactoryBot) and then to find the contact 
created to verify that what was entered on the UI was saved as expected.

To specify a record_type, call the `set_record_type` method after the `visit` method.
The default record type can be used by calling visit with
```ruby
visit(use_record_type: false)
```

> See create spec for example

### Manually specify label

Sometimes the label on the UI does not match the metadata label. In that case, one case manually
specify the type of object, the label and what to set it to. E.g.,

```ruby
UpdateAccountPage.visit(@account.id).set_picklist("Type", Account::AccountType.prospect).save
```

This will set a picklist with the label `Type` to the value defined by the picklist `AccountType`, and
the value `prospect`.

Follow other examples in `spec` to build tests using the auto-generated classes.

> These tests require that you [disable 2 step authentication for users](https://gitlab.com/leap-dojo/leap_salesforce_ui/-/wikis/Disable-2-step-authentication-and-allow-IP-range)
unless run from an IP address that your Salesforce Org trusts. Many shared CI environments have runners that would
be hard to whitelist as the address is dynamic.

### Options

To run tests in headless mode, set headless mode to true.
```ruby
LeapSalesforce.headless = true
```

### Authentication
If IP address for user profile is not allowed you will need to use a security token. See
leap_salesforce for how to set this for each user's key through environment variables. 

### Helpful commands

Go to Salesforce instance defined by logging in
```ruby
LeapSalesforce.browser.goto SoqlHandler.instance_url
```

Login through SOAP session to get information for browser
```ruby
user = LeapSalesforce::Users.where username: LeapSalesforce.ui_user
session = LeapSalesforce::Session.new user.username, LeapSalesforce.password,
                                      user.security_token
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. 
Then, run `rake spec` to run the tests. 
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. 
To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitLab at https://gitlab.com/leap-dojo/leap_salesforce_ui. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/leap-dojo/leap_salesforce_ui/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the LeapSalesforceUi project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/leap-dojo/leap_salesforce_ui/blob/master/CODE_OF_CONDUCT.md).
