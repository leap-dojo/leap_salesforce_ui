# frozen_string_literal: true

# Add users to LeapSalesforce context. First user is the default
module LeapSalesforce
  Users.add User.new :admin, "samuel.garratt@brave-otter-ttxype.com"
  Users.add User.new :not_there, "not.there@brave-otter-ttxype.com"
  Users.add User.new :cet, "test.cet@brave-otter-ttxype.com"
end

ENV["SF_USERNAME"] = "samuel.garratt@brave-otter-ttxype.com"
