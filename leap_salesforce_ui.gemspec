# frozen_string_literal: true

require_relative "lib/leap_salesforce_ui/version"

Gem::Specification.new do |spec|
  spec.name          = "leap_salesforce_ui"
  spec.version       = LeapSalesforceUi::VERSION
  spec.authors       = ["IQA", "Samuel Garratt"]
  spec.email         = %w[samuel.garratt@integrationqa.com leap.salesforce@integrationqa.com]

  spec.summary       = 'Helps with setting up integrated automated test frameworks
                        against Salesforce UI.'
  spec.description   = 'This gem helps ones to perform integration tests on Salesforce.
It reads the Metadata from Salesforce and creates the foundation for UI tests.'
  spec.homepage      = "https://gitlab.com/leap-dojo/leap_salesforce_ui"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.5")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/leap-dojo/leap_salesforce_ui"
  spec.metadata["changelog_uri"] = "https://gitlab.com/leap-dojo/leap_salesforce_ui/ChangeLog"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:spec)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "leap_salesforce", ">= 1.2.1"
  spec.add_dependency "watir", "7"
  spec.add_dependency "webdrivers"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
