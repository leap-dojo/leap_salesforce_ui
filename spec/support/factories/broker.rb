# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :broker do
    broker_name { Faker::Lorem.paragraph_by_chars(number: 80) }
    trait :all do
      record_id { "Best to not hard code this" }
      owner_id { User.find(CreatedDate: "<2021-02-16 17:54:14 +1300").id }
      deleted { true }
      created_date { "Content depending on datetime" }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { "Content depending on datetime" }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { "Content depending on datetime" }
      last_viewed_date { "Content depending on datetime" }
      last_referenced_date { "Content depending on datetime" }
      broker_id { "Content depending on double" }
      email { "Content depending on email" }
      mobile_phone { "Content depending on phone" }
      phone { "Content depending on phone" }
      picture { Faker::Lorem.paragraph_by_chars(number: 1300) }
      picture { "Content depending on url" }
      title { Faker::Lorem.paragraph_by_chars(number: 30) }
    end
  end
end
