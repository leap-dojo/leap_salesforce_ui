# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for RecordType mapped from SOQL RecordType
class RecordType < SoqlData
  # List of accessors for retrieving and setting fields in RecordType
  module Fields
    extend SoqlGlobalObjectData
    
    # Element for 'Record Type ID', type 'id'
    soql_element :record_type_id, 'Id'
    
    # Element for 'Name', type 'string'
    soql_element :name, 'Name'
    
    # Element for 'Record Type Name', type 'string'
    soql_element :record_type_name, 'DeveloperName'
    
    # Element for 'Namespace Prefix', type 'string'
    soql_element :namespace_prefix, 'NamespacePrefix'
    
    # Element for 'Description', type 'string'
    soql_element :description, 'Description'
    
    # Element for 'Business Process ID', type 'reference'
    soql_element :business_process_id, 'BusinessProcessId'
    
    # Element for 'SObject Type Name', type 'picklist'
    soql_element :s_object_type_name, 'SobjectType'
    
    # Element for 'Active', type 'boolean'
    soql_element :active, 'IsActive'
    
    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'
    
    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'
    
    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'
    
    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'
    
    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'
    
  end
end