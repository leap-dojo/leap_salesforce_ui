# frozen_string_literal: true

require_relative "broker_field_names"
# An Broker object mapping to a SOQL Broker__c
class Broker < SoqlData
  include Broker::Fields
  soql_object "Broker__c"
end
