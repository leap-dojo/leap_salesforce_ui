# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Broker mapped from SOQL Broker__c
class Broker < SoqlData
  # List of accessors for retrieving and setting fields in Broker
  module Fields
    extend SoqlGlobalObjectData
    
    # Element for 'Record ID', type 'id'
    soql_element :record_id, 'Id'
    
    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'
    
    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'
    
    # Element for 'Broker Name', type 'string'
    soql_element :broker_name, 'Name'
    
    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'
    
    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'
    
    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'
    
    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'
    
    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'
    
    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'
    
    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'
    
    # Element for 'Broker Id', type 'double'
    soql_element :broker_id, 'Broker_Id__c'
    
    # Element for 'Email', type 'email'
    soql_element :email, 'Email__c'
    
    # Element for 'Mobile Phone', type 'phone'
    soql_element :mobile_phone, 'Mobile_Phone__c'
    
    # Element for 'Phone', type 'phone'
    soql_element :phone, 'Phone__c'
    
    # Element for 'Picture', type 'string'
    soql_element :picture, 'Picture_IMG__c'
    
    # Element for 'Picture', type 'url'
    soql_element :picture, 'Picture__c'
    
    # Element for 'Title', type 'string'
    soql_element :title, 'Title__c'
    
  end
end