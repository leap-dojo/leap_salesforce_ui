require_relative 'record_type_field_names'
# An RecordType object mapping to a SOQL RecordType
class RecordType < SoqlData
  include RecordType::Fields
  
end
