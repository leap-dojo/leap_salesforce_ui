# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Contact, Salutation
class Contact < SoqlData
  # Enumeration for Salutation
  module Salutation
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @mr = "Mr."

    @ms = "Ms."

    @mrs = "Mrs."

    @dr = "Dr."

    @prof = "Prof."

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Salutation"
      end

      attr_reader :mr, :ms, :mrs, :dr, :prof
    end
  end
end
