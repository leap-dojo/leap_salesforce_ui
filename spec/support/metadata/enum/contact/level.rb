# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Contact, Level
class Contact < SoqlData
  # Enumeration for Level
  module Level
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @secondary = "Secondary"

    @tertiary = "Tertiary"

    @primary = "Primary"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Level"
      end

      attr_reader :secondary, :tertiary, :primary
    end
  end
end
