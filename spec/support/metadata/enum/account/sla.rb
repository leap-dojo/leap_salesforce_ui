# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, SLA
class Account < SoqlData
  # Enumeration for SLA
  module SLA
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @gold = "Gold"

    @silver = "Silver"

    @platinum = "Platinum"

    @bronze = "Bronze"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "SLA"
      end

      attr_reader :gold, :silver, :platinum, :bronze
    end
  end
end
