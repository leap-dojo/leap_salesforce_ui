# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, UpsellOpportunity
class Account < SoqlData
  # Enumeration for UpsellOpportunity
  module UpsellOpportunity
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @maybe = "Maybe"

    @no = "No"

    @yes = "Yes"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Upsell Opportunity"
      end

      attr_reader :maybe, :no, :yes
    end
  end
end
