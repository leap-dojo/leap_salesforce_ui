# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, Active
class Account < SoqlData
  # Enumeration for Active
  module Active
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @no = "No"

    @yes = "Yes"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Active"
      end

      attr_reader :no, :yes
    end
  end
end
