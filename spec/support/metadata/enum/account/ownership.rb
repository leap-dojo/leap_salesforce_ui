# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, Ownership
class Account < SoqlData
  # Enumeration for Ownership
  module Ownership
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @public = "Public"

    @private = "Private"

    @subsidiary = "Subsidiary"

    @other = "Other"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Ownership"
      end

      attr_reader :public, :private, :subsidiary, :other
    end
  end
end
