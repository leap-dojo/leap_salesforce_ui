# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, AccountRating
class Account < SoqlData
  # Enumeration for AccountRating
  module AccountRating
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @hot = "Hot"

    @warm = "Warm"

    @cold = "Cold"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Account Rating"
      end

      attr_reader :hot, :warm, :cold
    end
  end
end
