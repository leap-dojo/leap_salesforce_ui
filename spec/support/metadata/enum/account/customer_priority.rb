# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, CustomerPriority
class Account < SoqlData
  # Enumeration for CustomerPriority
  module CustomerPriority
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @high = "High"

    @low = "Low"

    @medium = "Medium"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Customer Priority"
      end

      attr_reader :high, :low, :medium
    end
  end
end
