# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, CleanStatus
class Account < SoqlData
  # Enumeration for CleanStatus
  module CleanStatus
    include SoqlEnum
    extend LeapSalesforce::CommonEnumMethods

    @in_sync = "In Sync"

    @different = "Different"

    @reviewed = "Reviewed"

    @not_found = "Not Found"

    @inactive = "Inactive"

    @not_compared = "Not Compared"

    @select_match = "Select Match"

    @skipped = "Skipped"

    class << self
      # @return [String] Name of picklist as returned from Metadata
      def name
        "Clean Status"
      end

      attr_reader :in_sync, :different, :reviewed, :not_found, :inactive, :not_compared, :select_match, :skipped
    end
  end
end
