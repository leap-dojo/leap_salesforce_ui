# frozen_string_literal: true

class ViewContactPage < ViewPage
  soql_object Contact
end
