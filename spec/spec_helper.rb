# frozen_string_literal: true

require_relative "../config/credentials/token" if File.exist? 'config/credentials/token.rb'
require "leap_salesforce_ui"

Soaspec::OAuth2.debug_oauth = false
LeapSalesforce.ui_user = :admin
LeapSalesforce.headless = true
LeapSalesforce.login_through_session = true

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:each) do |example|
    # Start browser with test name as metadata
    LeapSalesforce.browser example.full_description
  end

  config.after(:each) do |example|
    if example.exception
      LeapSalesforce.browser.screenshot.save "logs/#{tidy_description(example.full_description.to_s)}_failure.png"
    end
    LeapSalesforce.close_browser # Fresh browser for each test
  end

  config.after(:suite) { sleep 20 } if ENV["remote"] == "true" # Wait for video recording to finish
end

# Tidy RSpec example description so it's suitable for a screenshot name
# @return [String] Description of test suitable for file name
def tidy_description(rspec_description)
  rspec_description.gsub("#<RSpec::Core::Example", "").tr('>".:{}', "").tr("=", "").tr(" ", "")
end
