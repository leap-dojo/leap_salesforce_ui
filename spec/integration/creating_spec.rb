# frozen_string_literal: true

RSpec.describe "Creating Contact" do
  let(:first_name) { Faker::Name.first_name }
  let(:last_name) { Faker::Name.last_name }
  let(:salutation) { Contact::Salutation.sample }
  let(:other_street) { Faker::Address.full_address }
  it "populate form and save" do
    account = FactoryBot.create(:account)
    CreateContactPage.visit(use_record_type: true)
        .set_record_type('Competitor')
        .submit_with({ "First Name" => first_name,
                                          last_name: last_name,
                                          salutation: salutation,
                                          other_street: other_street,
                                          account_id: account.account_name })
    contact_id = ViewContactPage.id
    @contact = Contact.find(id: contact_id)
    expect(@contact.first_name).to eq first_name
    expect(@contact.last_name).to eq last_name
    expect(@contact.salutation).to eq salutation
    expect(@contact.other_street).to eq other_street
    expect(@contact.account_id).to eq account.id
  end
  it "shows error for missing mandatory fields" do
    expect do
      CreateContactPage.visit(use_record_type: false)
          .submit_with({ "First Name" => first_name })
    end.to raise_error LeapSalesforce::SubmitError, /Errors creating on CreateContactPage.*Name/
  end
  after(:each) do
    @contact&.delete # Don't want to fill sandbox with data
  end
end

RSpec.describe "Creating Broker" do
  let(:name) { Faker::Name.first_name }
  it "can create with a name" do
    CreateBrokerPage.visit.populate_with(broker_name: name)
    CreateBrokerPage.save
    broker_id = ViewBrokerPage.id
    @broker = Broker.find id: broker_id
    expect(@broker.broker_name).to eq name
  end
  it "fails with helpful error when setting field fails" do
    Watir.default_timeout = 5
    expect do
      CreateBrokerPage.visit.populate_with owner_id: "Missing label"
    end.to raise_error LeapSalesforce::SetFieldError
  end
  after(:each) do
    Watir.default_timeout = 30
    @broker&.delete # Don't want to fill sandbox with data
  end
end
