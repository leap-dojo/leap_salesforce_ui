# frozen_string_literal: true

RSpec.describe "Updating record" do
  let(:number) { Faker::Number.number digits: 5 }
  it "can update value" do
    @account = FactoryBot.create(:account)
    UpdateAccountPage.visit(@account.id).populate_with(account_number: number)
                     .set_picklist "Type", Account::AccountType.prospect
    UpdateAccountPage.save
    # TODO: Understand why label in metadata does not match UI name
    updated_account = Account.find id: @account.id
    expect(updated_account.account_number).to eq number.to_s
    expect(updated_account.account_type).to eq Account::AccountType.prospect
  end
  after(:each) do
    @account&.delete # Don't want to fill sandbox with data
  end
end
