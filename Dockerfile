FROM ruby:2.7
# Issue as of this time with Selenium webdriver remote with Ruby3
MAINTAINER Samuel Garratt

# Required ruby gems
RUN mkdir /mysuite
WORKDIR /mysuite
COPY Gemfile /mysuite/Gemfile
COPY leap_salesforce_ui.gemspec /mysuite/leap_salesforce_ui.gemspec
COPY lib/leap_salesforce_ui/version.rb /mysuite/lib/leap_salesforce_ui/version.rb
RUN gem install bundler
RUN bundle install

# Add language settings to handle special characters
RUN export LANG=C.UTF-8
RUN export LANGUAGE=C.UTF-8
RUN export LC_ALL=C.UTF-8
