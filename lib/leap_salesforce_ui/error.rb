# frozen_string_literal: true

module LeapSalesforce
  # Error related to submitting a form
  class SubmitError < Error; end

  # Error related to setting a field
  class SetFieldError < Error; end
end
