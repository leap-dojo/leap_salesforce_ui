module LeapSalesforce
  module SetRecordType
    # Set record type for an object before create form appears
    def set_record_type(record_type)
      LeapSalesforce.logger.info "Setting record type to '#{record_type}'"
      option = browser.label(xpath: "//label[.//*[@class='slds-form-element__label' and text()='#{record_type}']]")
      option.click
      browser.button(text: 'Next').click
      self
    end
  end
end
