# frozen_string_literal: true

require_relative "../leap_salesforce_ui"
Dir.glob(File.join(__dir__, "rake", "*.rake")).each(&method(:import))
