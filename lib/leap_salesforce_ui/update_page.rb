# frozen_string_literal: true

# Base class for updating an entity in Salesforce
class UpdatePage
  extend BasePage
  extend LeapSalesforce::PageIntrospection
  extend LeapSalesforce::FormFiller

  class << self
    def visit(id)
      LoginPage.login
      browser.goto "#{SoqlHandler.instance_url}/lightning/r/#{@soql_object.soql_object_name}/#{id}/edit"
      self
    end
  end
end
