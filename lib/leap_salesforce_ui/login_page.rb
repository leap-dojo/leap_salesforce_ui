# frozen_string_literal: true

class LoginPage
  class << self
    DISABLE_2STEP_URL = "https://gitlab.com/leap-dojo/leap_salesforce_ui/-/wikis/Disable-2-step-authentication-and-allow-IP-range"

    # @return [Watir::Browser]
    def browser
      LeapSalesforce.browser
    end

    def error_message_element
      browser.div(id: "error")
    end

    def error_message
      error_message_element.text
    end

    def error_message?
      error_message_element.present?
    end

    # Could be used if a user wants to login through UI for login specific tests
    # which should not be necessary
    def login_manually
      browser.goto LeapSalesforce.general_url
      browser.text_field(id: "username").set(LeapSalesforce.ui_user)
      browser.text_field(id: "password").set(LeapSalesforce.password)
      browser.button(id: "Login").click
      Watir::Wait.until(timeout: 60, message: "Did not login within the expected time") do
        raise "Cannot Login. #{error_message}" if error_message?

        if browser.url.include? "_ui/identity/verification/"
          raise LeapSalesforce::SetupError, "2 step verification page appears.
Go to #{DISABLE_2STEP_URL} to learn how to disable it"
        end

        !browser.url.include? LeapSalesforce.general_url
      end
    end

    def login
      raise "Need to set LeapSalesforce.ui_user" unless LeapSalesforce.ui_user
      LeapSalesforce.logger.info "Logging in as user '#{LeapSalesforce.ui_user}'"
      if LeapSalesforce.login_through_session
        browser.goto SoqlHandler.instance_url
        user = LeapSalesforce::Users.where username: LeapSalesforce.ui_user
        session = LeapSalesforce::Session.new user.username, LeapSalesforce.password, user.security_token
        browser.cookies.add "sid", session.session_id
        browser.cookies.add "sidClient", session.user_id
        browser.goto SoqlHandler.instance_url
      else
        browser.goto "#{LeapSalesforce.general_url}/?un=#{LeapSalesforce.ui_user}&pw=#{LeapSalesforce.password}"
      end
      continue_button = browser.button(id: "thePage:inputForm:continue")
      if continue_button.exists?
        browser.checkbox(id: "thePage:inputForm:remember").set
        continue_button.click
      end
    end
  end
end
