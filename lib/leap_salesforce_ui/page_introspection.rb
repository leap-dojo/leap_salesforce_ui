# frozen_string_literal: true

module LeapSalesforce
  # Relates to a page inspecting it's own state
  module PageIntrospection
    # @return [String] Id of current entity
    def id
      LeapSalesforce.browser.url.split("/")[-2]
    end
  end
end
