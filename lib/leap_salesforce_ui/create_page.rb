# frozen_string_literal: true
require_relative 'set_record_type'

# Base class for creating entities in Salesforce
class CreatePage
  extend BasePage
  extend LeapSalesforce::FormFiller
  extend LeapSalesforce::SetRecordType
end
