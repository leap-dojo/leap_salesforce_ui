# frozen_string_literal: true

module LeapSalesforce
  module FormFiller
    # @return [Watir::Elements::Div] Div where form entered to create/edit entity
    def form
      browser.div(class: "inlinePanel")
    end

    # Use metadata to get label for field
    # @param [String] field
    def get_label_for(field)
      raise "SoqlObject not defined" unless @soql_object

      return field if @soql_object.label_names.include? field

      raise "Cannot find field #{field} on #{@soql_object}" unless @soql_object.accessors[field.to_sym]

      @soql_object.accessors[field.to_sym][:label]
    end

    # @return [Hash] Description of field
    def field_for(desc)
      ruby_desc = @soql_object.accessors[desc.to_sym]
      return ruby_desc if ruby_desc

      @soql_object.properties_for(desc)
    end

    # Based on data type of field passed, fill in each field
    # The type of field is attempted to be filled in based on the metadata
    # @param [Hash] data Hash with label => value to set syntax.
    def populate_with(data)
      sleep 1 # Being too fast can cause issues with first value. TODO: Wait for a condition
      data.each do |desc, value|
        field = field_for(desc)
        label_name = field[:label] || field["label"]

        type = field[:type] || field["type"]
        case type
        when "string" then set_text_field(label_name, value)
        when "picklist" then set_picklist(label_name, value)
        when "textarea" then set_text_area(label_name, value)
        when "reference" then set_reference_field(label_name, value)
        else
          raise NotImplementedError, "#{type} not yet defined in #{self}"
        end
      end
      self
    end

    # Set input text field
    # @param [String] label Label of textfield
    # @param [String] value Value to set textfield to
    def set_text_area(label, value)
      LeapSalesforce.logger.info "Setting text area, label '#{label}' with '#{value}'"
      field_transaction label, value do
        text_area = form.textarea(xpath: "//lightning-textarea[./label[contains(.,'#{label}')]]//following-sibling::div/textarea")
        text_area.set value
      end
    end

    # Set input text field
    # @param [String] label Label of text field
    # @param [String] value Value to set text field to
    def set_text_field(label, value)
      value = value.to_s
      LeapSalesforce.logger.info "Setting text field, label '#{label}' with '#{value}'"
      field_transaction label, value do
        label_element = form.label(xpath: "//label[contains(.,'#{label}')]")
        text_field = label_element.text_field(xpath: ".//following-sibling::input|.//following-sibling::div/input")
        text_field.focus
        text_field.set! value
        return self if text_field.value == value

        sleep 2 # Wait a bit and then set field
        text_field.set! value
        unless text_field.value == value
          raise SetFieldError, "Unable to set text field '#{label}' with value " \
                               "'#{value}'. Value set is '#{text_field.value}'"
        end
      end
      self
    end

    # Set value of picklist
    # @param [String] label Label of picklist
    # @param [String] value Value to set picklist to
    def set_picklist(label, value)
      LeapSalesforce.logger.info "Setting picklist, label '#{label}' with '#{value}'"
      field_transaction label, value do
        dropdown = form.button(xpath: "//*[./*[text()='#{label}']]//following-sibling::div//button")
        dropdown_id = dropdown.attribute "id"
        LeapSalesforce.logger.debug "Using dropdown id: #{dropdown_id}"
        dropdown.focus
        sleep 0.5
        dropdown.click
        has_value = "@data-value='#{value}'"
        in_dropdown = "starts-with(@data-item-id, '#{dropdown_id}')"
        browser.element(xpath: ".//lightning-base-combobox-item[#{has_value} and #{in_dropdown}]").click
      end
      self
    end

    # Set reference field where another entity is referred to by this entity
    def set_reference_field(label, value)
      ref_label = label.gsub("ID", "Name")
      search_val = value[0..12]
      LeapSalesforce.logger.info "Setting reference field, label '#{ref_label}', searching with '#{search_val}'"
      field_transaction label, value do
        search_field = browser.text_field(xpath: "//label[contains(.,'#{ref_label}')]//following-sibling::div[1]//input")
        search_field.set search_val
        browser.element(xpath: ".//lightning-base-combobox-item[starts-with(.,'SearchShow')]").click
        browser.link(xpath: "//div[contains(@class,'searchScroller')]//a[contains(.,'#{search_val}')]").click
      end
      self
    end

    def submit_with(data)
      populate_with data
      save
    end

    # @return [Watir::Elements::Ul] List of errors
    def error_list
      browser.ul(xpath: "//ul[contains(@class, 'errorsList') and not(normalize-space()='')]")
    end

    # Save the current form, creating the new object
    def save
      LeapSalesforce.logger.info "Saving form for #{self}"
      form.button(xpath: "//button[@title='Save']|//button[text()='Save']").click
      form.wait_until do |panel|
        sleep 1.5
        if error_list.exists?
          errors = error_list.lis
          error_text = errors.collect(&:text)
          LeapSalesforce.logger.error "Error submitting #{self} #{error_list.text}"
          raise LeapSalesforce::SubmitError, "Errors creating on #{self}: #{error_text}" if errors.count.positive?
        end

        !panel.present?
      end
    end

    private

    # Process of setting a field within this block. Exceptions will be caught
    # and thrown with context specific information
    def field_transaction(label, value)
      yield
    rescue Watir::Exception::Error => e
      raise LeapSalesforce::SetFieldError, "Unable to set label '#{label}' to '#{value}'" \
                                           " on #{self}. Due to #{e.inspect}"
    end
  end
end
