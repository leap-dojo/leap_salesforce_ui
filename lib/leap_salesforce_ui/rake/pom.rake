# frozen_string_literal: true

namespace :leaps do
  desc "Create Page objects using leaps ui framework"
  task :create_poms do
    require_relative "../generator/page_objects"
    LeapSalesforce::Generator::PageObjects.new.create
  end
end
