# frozen_string_literal: true

require "leap_salesforce/generator/generator"

module LeapSalesforce
  # Generators for creating code
  module Generator
    # Creates SoqlObjects and related modules
    class PageObjects
      include Generator

      POM_FOLDER = File.join(LeapSalesforce.lib_folder, "page_objects").freeze

      def create
        LeapSalesforce.objects_to_verify.each { |entity| create_poms_for entity }
      end

      # @param [LeapSalesforce::SoqlData] entity An object representing an object in Salesforce
      def create_poms_for(entity)
        @entity_name = entity
        @soql_object = LeapSalesforce.soql_objects.find { |so| so.class_name == entity.to_s }
        %w[create view update].each do |page_action|
          content = read_template "#{page_action}_page.rb.erb", binding, folder: __dir__
          file = File.join(POM_FOLDER, "#{@entity_name.to_s.snakecase}/#{page_action}_page.rb")
          generate_file file, content, overwrite: false
        end
      end
    end
  end
end
