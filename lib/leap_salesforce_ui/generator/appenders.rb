# frozen_string_literal: true

require "leap_salesforce/generator/generator"

module LeapSalesforce
  module Generators
    # Used for appending content onto existing files (or adding if not already present)
    module Appenders
      include LeapSalesforce::Generator

      # Create content in a file, adding to an existing file if present
      def append(filename, template_path)
        verb = "Appending to"
        unless File.exist? filename
          FileUtils.touch filename
          verb = "Creating"
        end
        content = read_template template_path, binding, folder: __dir__
        if File.read(filename).include?(content)
          puts "File '#{filename}' already has expected content, skipping...".colorize :red
        else
          puts "\u2713 #{verb} #{filename}".colorize :green
          open(filename, "a") { |f| f.puts content }
        end
      end
    end
  end
end
