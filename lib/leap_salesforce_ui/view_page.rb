# frozen_string_literal: true

# Base class for viewing an entity in Salesforce
class ViewPage
  extend BasePage
  extend LeapSalesforce::PageIntrospection
  class << self
    def visit(id)
      LoginPage.login
      browser.goto "#{SoqlHandler.instance_url}lightning/r/#{@soql_object.soql_object_name}/#{id}/view"
      self
    end
  end
end
