# frozen_string_literal: true

require_relative "login_page"

module BasePage
  # @return [Watir::Browser]
  def browser
    LeapSalesforce.browser
  end

  # Set entity this page object refers to
  # @param [Class] soql_object Backend name of SoqlObject this page object refers to
  def soql_object(soql_object)
    @soql_object = soql_object
  end

  # Visit the current page, logging in if required
  # @param [Boolean] use_record_type Whether to specify record type
  def visit(use_record_type: true)
    LoginPage.login
    page_url = "#{SoqlHandler.instance_url}/lightning/o/#{@soql_object.soql_object_name}/new"
    page_url += '?useRecordTypeCheck=1' if use_record_type
    LeapSalesforce.logger.info "Visiting #{self}"
    browser.goto page_url
    self
  end
end
