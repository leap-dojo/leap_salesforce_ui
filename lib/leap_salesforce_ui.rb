# frozen_string_literal: true

require "watir"
require "webdrivers"
require "leap_salesforce"
require_relative "leap_salesforce_ui/version"
require_relative "leap_salesforce_ui/error"
require_relative "leap_salesforce_ui/base_page"
require_relative "leap_salesforce_ui/page_introspection"
require_relative "leap_salesforce_ui/form_filler"
require_relative "leap_salesforce_ui/create_page"
require_relative "leap_salesforce_ui/view_page"
require_relative "leap_salesforce_ui/update_page"
page_obj_folder = File.join(LeapSalesforce.lib_folder, "page_objects")
require_all page_obj_folder if Dir.exist? page_obj_folder

# @return [Hash]
def zalenium_args(feature_name, scenario)
  args = { timeout: 120, url: ENV["WEBDRIVER_URL"], name: "Scenario: #{scenario} #{Time.now}",
           'zal:build': "Feature: #{feature_name}" }
  case ENV["BROWSER"]
  when "chrome"
    args.merge!('goog:chromeOptions': {
                  args: ENV["WEBDRIVER_CHROMEOPTIONS"]&.split(" ") || %w[]
                })
  when "firefox"
    args.merge!(timeouts: 120)
  end
  args
end

# Adding additional UI capability to leap_salesforce gem
module LeapSalesforce
  # @return [String] User for UI
  @ui_user = nil
  # @return [Boolean] Whether to run in headless mode
  @headless = false
  # @return [Boolean] Whether to login using session obtained via SOAP
  @login_through_session = false
  class << self
    def browser(test_name = nil)
      @browser ||= new_browser(test_name)
    end

    def close_browser
      @browser&.close
      @browser = nil
    end

    # @return [String] User used for UI tests
    attr_reader :ui_user

    # @return [Boolean] Whether to run in headless mode. Default false
    attr_accessor :headless
    # @return [Boolean] Whether to login using session obtained via SOAP
    attr_accessor :login_through_session

    # @param [String, Symbol, Regexp, LeapSalesforce::User] user User or email address of user
    def ui_user=(user)
      @ui_user = if user.is_a? String
                   user
                 else
                   LeapSalesforce::Users.where(user)&.username
                 end
      Soaspec::SpecLogger.info "Using user '#{@ui_user}' for UI"
    end

    private

    def new_browser(test_name = nil)
      if ENV["WEBDRIVER_URL"]
        # TODO: Get working on Ruby 3
        Watir::Browser.new :chrome, options: { **zalenium_args("LeapSalesforce", test_name || "Test") }
      else
        args = {}
        args[:headless] = true if headless
        Watir::Browser.new :chrome, args
      end
    end
  end
end
